import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

// Child component
import {CanvasComponent} from './canvas/canvas.component';

import { AppComponent } from './app.component';

@NgModule({
  declarations: [
    AppComponent,
    CanvasComponent
  ],
  imports: [
    BrowserModule
  ],
  providers: [],
  bootstrap: [
    AppComponent,
  ]
})
export class AppModule { }
