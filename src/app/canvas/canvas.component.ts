import {Component, OnInit} from '@angular/core';
import {element} from "protractor";

const MIN_VEL_PERC = 0.6;
const PARTICLES_IN_SQUARE = 5;

class Particle {
  id: number;
  x: number;
  y: number;
  velX: number;
  velY: number;
  sector?: string
}
class CordObj{
  x: number;
  y: number;
}

@Component({
  selector: 'canvas-panel',
  templateUrl: './canvas.component.html',
  styleUrls: ['./canvas.component.css']
})
export class CanvasComponent implements OnInit {

  private displayCanvas: HTMLCanvasElement;
  private context;
  private centerX: number;
  private centerY: number;
  private boundaryRad: number;
  private particleList: Particle[] = [];
  private numParticles: number;
  private displayWidth: number;
  private displayHeight: number;
  private display: CordObj;

  private particleColor: string;
  private particleRad: number;
  private boundaryRadSquare;
  private spriteSheetCanvas;
  private spriteSheetContext;
  private exitX;
  private exitY;
  private exitRad;
  private vx;
  private vy;
  private twiceProjFactor;
  private coreRad: number;
  private spriteCircleInnerRad;
  private spriteCircleOuterRad;
  private spriteCircleOuterDiam;
  private radSquare;
  private initVelMax;
  private maxVelComp;
  private particleDiam: number;
  private randAccel;
  private temp;
  private haloEdgeColor: string;
  private haloOuterColor: string;
  private oldSectors = {};
  private oldSectorsSquares: CordObj;

  constructor() {
  }

  ngOnInit() {
    this.canvasInit();
  }

  canvasInit() {

    this.displayCanvas = <HTMLCanvasElement>document.getElementById("panel");
    this.context = this.displayCanvas.getContext("2d");

    this.numParticles = 100;

    this.particleColor = "#FFFFFF";
    this.haloEdgeColor = "rgba(255,200,50,0.4)";
    this.haloOuterColor = "rgba(255,200,50,0)";

    this.particleRad = 24;
    this.coreRad = 5;
    this.particleDiam = 2 * this.particleRad;
    this.display = {
      x: this.displayCanvas.clientWidth,
      y: this.displayCanvas.clientHeight
    };

    this.centerX = this.display.x / 2;
    this.centerY = this.display.y / 2;
    this.boundaryRad = 0.5 * Math.min(this.display.x, this.display.y) - this.particleRad - 1;
    this.boundaryRadSquare = this.boundaryRad * this.boundaryRad;

    this.initVelMax = 3.5;
    this.maxVelComp = 3.5;
    this.randAccel = 0.2;

    this.oldSectorsSquares = {
      x: Math.round(this.display.x / (PARTICLES_IN_SQUARE * this.coreRad * 2)),
      y: Math.round(this.display.y / (PARTICLES_IN_SQUARE * this.coreRad * 2))
    };

    this.createParticles();
    console.dir(this.particleList);
    console.dir(this.oldSectors);

    // this.makethisSpriteSheet();

    this.context.fillStyle = "#000000";
    this.context.fillRect(0, 0, this.display.x, this.display.y);

    setInterval(() => {
      this.onTimer()
    }, 1000 / 40);

  }

  createParticles() {
    let vAngle;
    let vMag;
    let allX = {};
    let allY = {};
    for (let i = 0; i < this.numParticles; i++) {
      vAngle = Math.random() * 2 * Math.PI;
      vMag = this.initVelMax * (MIN_VEL_PERC + (1 - MIN_VEL_PERC) * Math.random());


      let xCord = this.display.x * Math.random(),
        yCord = this.display.y * Math.random();

      let newParticle = {
        id: i,
        x: xCord,
        y: yCord,
        velX: vMag * Math.cos(vAngle),
        velY: vMag * Math.sin(vAngle)
      };

      while (allX.hasOwnProperty(Math.round(xCord))) {
        newParticle.x = xCord;
        xCord = this.display.x * Math.random();
      }

      while (allY.hasOwnProperty(Math.round(yCord))) {
        newParticle.y = yCord;
        yCord = this.display.y * Math.random();
      }

      this.sectorFiller(newParticle);

      this.particleList.push(newParticle);
    }
  }

  sectorFiller(particle){
    let sector = new CordObj();

    ['x', 'y'].forEach(key => {
      let squareSize = this.display[key] / this.oldSectorsSquares[key];
      sector[key] = Math.floor(particle[key] / squareSize)
    });

    let filledSectorKey = JSON.stringify(sector);

    particle.sector = filledSectorKey;

    if (this.oldSectors[filledSectorKey]) {
      this.oldSectors[filledSectorKey].push(particle.id)
    } else {
      this.oldSectors[filledSectorKey] = [particle.id]
    }
  }

  onTimer() {


    this.context.fillStyle = '#000';
    this.context.fillRect(0, 0, this.display.x, this.display.y);
    this.context.fill();

    let lastX, lastY;
    //update and draw particles
    this.context.globalCompositeOperation = "lighter";
    let p = this.particleList;
    let newX, newY;

    for (let key in this.oldSectors){
      if (this.oldSectors.hasOwnProperty(key) && this.oldSectors[key].length > 1){
        this.oldSectors[key].forEach(particleIndex => {
        this.checkCollision(this.particleList[particleIndex])
        })
      }
    }

    this.oldSectors = {};

    p.forEach(elem => {

      this.sectorFiller(elem);

      let currX = Math.round(elem.x + elem.velX),
          currY = Math.round(elem.y + elem.velY);

      if (currX < this.particleRad){
        elem.x = this.particleRad;
        elem.velX *= -1
      } else if(currX > this.display.x - this.particleRad) {
        elem.x = this.display.x - this.particleRad;
        elem.velX *= -1
      }

      if (currY < this.particleRad){
        elem.y = this.particleRad;
        elem.velY *= -1
      } else if(currY > this.display.y - this.particleRad) {
        elem.y = this.display.y - this.particleRad;
        elem.velY *= -1
      }

      // this.checkCollision(elem);

      this.context.globalCompositeOperation = "source-over";
      this.context.beginPath();
      this.context.arc(elem.x += elem.velX, elem.y += elem.velY, 5, 0, 2 * Math.PI, false);
      this.context.fillStyle = '#fff';
      this.context.fill();
    });


  }

  checkCollision(particle){
    let particlesInSector = this.oldSectors[particle.sector];
    if (particlesInSector.length < 2) return;
    this.oldSectors[particle.sector].forEach(elem => {
      if(elem != particle.id && Math.sqrt(Math.pow(particle.x + particle.velX - this.particleList[elem].x - this.particleList[elem].velX, 2) + Math.pow(particle.y + particle.velY - this.particleList[elem].y - this.particleList[elem].velY, 2)) <= this.coreRad * 2){

       

        ['velX', 'velY'].forEach(key => {
          particle[key] = - (particle[key] - this.particleList[elem][key]) / 2;
          this.particleList[elem][key] = - particle[key];
        });

         particle.x += particle.velX;// + ((particle.velX / Math.abs(particle.velX) * .1 * this.coreRad));
        particle.y += particle.velY;// + ((particle.velY / Math.abs(particle.velY) * .1 * this.coreRad));

        this.particleList[elem].x += this.particleList[elem].velX;// + ((this.particleList[elem].velX / Math.abs(this.particleList[elem].velX) * .1 * this.coreRad));
        this.particleList[elem].y += this.particleList[elem].velY;// + ((this.particleList[elem].velY / Math.abs(this.particleList[elem].velY) * .1 * this.coreRad));
        // console.dir(`${particle.id}_____${this.particleList[elem].id}`);
      }
    })
  }

}

